# Neural-Network-Haskell

By Adiel Ahmad, Marco Herzog, Felix Brübach, Jannis Weber, Andor Willared

This is a project for the module "Functional Programming".

## Project
We have implemented a naive neural network in haskell.<br>
You can view the documentation [here](https://git.thm.de/akwl20/neural-network-haskell/blob/master/doc/index.html).<br> 
**Our GUI applications only run on Ubuntu!**<br>

### To use our network:

Clone repo, open 'window' under Ubuntu and load 'binary' (pretrained nerual net) into the application if you want to see a trained network. 

Or use our network module to use it for your own problem.
