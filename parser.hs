import MNIST
import System.Directory
import Data.Matrix

main = do
    contents <- getDirectoryContents "img"
    out <- sequence $ drop 2 (reverse [ parseToTupel content | content <- contents ])
    print out
    
parseToTupel :: FilePath -> IO (Matrix Float, Matrix Float)
parseToTupel path = do  
    png <- pngToVector ("img/"++path)
    let result = (png, fromList 3 1 (if(path!!0=='0') then ([1,0,0]) else (if(path!!0=='1') then ([0,1,0]) else ([0,0,1]))))
    return result
    
    
    